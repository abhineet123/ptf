function l = getLikelihood(f, a, b)
max_f=max(f);
d = ((max_f+b)./f) - 1;
l = exp(-a.*d.*d);
% l = exp(a.*f);
end


